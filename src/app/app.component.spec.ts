import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';
import { AppComponent } from './app.component';
import { AuthService } from './service/auth.service';


describe('AppComponent', () => {

  let app: AppComponent;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        RouterTestingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        FormsModule,
        HttpClientModule,
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const testapp = TestBed.createComponent(AppComponent);
    app = testapp.componentInstance;
    expect(app).toBeTruthy();
  });
  it('Test retrieve data from cloud function', async () => {
    const testapp = TestBed.createComponent(AppComponent);
    app = testapp.componentInstance;
    const res = await app.getData();

    console.log(res);

  });

/*

  it(`should have as title 'inpt-cloud-mamgas'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    expect(app.title).toEqual('inpt-cloud-mamgas');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('inpt-cloud-mamgas app is running!');
  });
  */
});

